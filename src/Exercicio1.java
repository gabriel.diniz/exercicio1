import java.util.Scanner;

public class Exercicio1 {

    public static void main(String[] args) { // método publico estatico principal

        exercicio1(); // metódo
    }

    public static void exercicio1() { // metódo

        Scanner in = new Scanner(System.in);

        int idadeAnos, idadeMeses, idadeDias, idadeTotalDias; // variaveis

        System.out.println("Insira os anos: "); // inserir anos
        idadeAnos = in.nextInt();

        System.out.println("Insira os meses: "); // inserir meses
        idadeMeses = in.nextInt();

        System.out.println("Insira os dias: "); // inserir dias
        idadeDias = in.nextInt();

        idadeTotalDias = idadeAnos * 365 + idadeMeses * 30 + idadeDias; // idade em anos vezes 365  mais idade em meses vezes 30 mais idade em dias
        System.out.println("Idade total em dias: " + idadeTotalDias); // imprimir idade total

    }

}
